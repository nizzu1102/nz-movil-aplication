// JavaScript Document

$('document').ready(function()
{ 
     /* validation */
	 $("#register-form").validate({
      rules:
	  {
			user_name: {
		    required: true,
			minlength: 3
			},
			password: {
			required: true,
			minlength: 8,
			maxlength: 15
			},
			cpassword: {
			required: true,
			equalTo: '#password'
			},
			user_email: {
            required: true,
            email: true
            },
	   },
       messages:
	   {
            user_name: "Por favor ingrese su usuario",
            password:{
                      required: "por favor ingrese una contraseña",
                      minlength: "La contraseña debe contener al menos 8 caracteres"
                     },
            user_email: "Por favor ingrese un e-mail válido",
			cpassword:{
						required: "Por favor reingrese su contraseña",
						equalTo: "Las contraseñas no coinciden !"
					  }
       },
	   submitHandler: submitForm	
       });  
	   /* validation */
	   
	   /* form submit */
	   function submitForm()
	   {		
				var data = $("#register-form").serialize();
				
				$.ajax({
				
				type : 'POST',
				url  : 'php-process/register.php',
				data : data,
				beforeSend: function()
				{	
					$("#error").fadeOut();
					$("#btn-submit").html('<i class="material-icons">swap_horiz</i> &nbsp; Enviando ...');
				},
				success :  function(data)
						   {						
								if(data==1){
									
									$("#error").fadeIn(1000, function(){
											
											
											$("#error").html('<div class="alert alert-danger"> <i class="material-icons">info</i> &nbsp; Lo sentimos, correo electrónico ya está ocupado !</div>');
											
											$("#btn-submit").html('<i class="material-icons">input</i> &nbsp; Crear cuenta');
										
									});
																				
								}
								else if(data=="registered")
								{
									
									$("#btn-submit").html('<img src="images/btn-ajax-loader.gif" /> &nbsp; Iniciando sesión ...');
									setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("../php-process/success.php"); }); ',5000);

									
								}
								else{
										
									$("#error").fadeIn(1000, function(){
											
						$("#error").html('<div class="alert alert-success"><i class="material-icons">info</i> &nbsp; '+data+' !</div>');
											
									$("#btn-submit").html('<i class="material-icons">input</i> &nbsp; Crear cuenta');
									setTimeout(' window.location.href = "index.php"; ',4000);
										
									});
											
								}
						   }
				});
				return false;
		}
	   /* form submit */
	   
	   
	 

});

/*
Author: Pradeep Khodke
URL: http://www.codingcage.com/
*/

$('document').ready(function()
{ 
     /* validation */
	 $("#login-form").validate({
      rules:
	  {
			password: {
			required: true,
			},
			user_email: {
            required: true,
            email: true
            },
	   },
       messages:
	   {
            password:{
                      required: "Por favor ingrese su contraseña"
                     },
            user_email: "Por favor ingrese su email",
       },
	   submitHandler: submitForm	
       });  
	   /* validation */
	   
	   /* login submit */
	   function submitForm()
	   {		
			var data = $("#login-form").serialize();
				
			$.ajax({
				
			type : 'POST',
			url  : 'php-process/login_process.php',
			data : data,
			beforeSend: function()
			{	
				$("#error").fadeOut();
				$("#btn-login").html('<i class="material-icons">swap_horiz</i> &nbsp; Enviando ...');
			},
			success :  function(response)
			   {						
					if(response=="ok"){
									
						$("#btn-login").html('<img src="images/btn-ajax-loader.gif" /> &nbsp; Iniciando sesión ...');
						setTimeout(' window.location.href = "../mapa.php"; ',4000);
					}
					else{
									
						$("#error").fadeIn(1000, function(){						
				$("#error").html('<div class="alert alert-danger"> <i class="material-icons">info</i> &nbsp; '+response+' !</div>');
											$("#btn-login").html('<i class="material-icons">input</i> &nbsp; Regístrate');
									});
					}
			  }
			});
				return false;
		}
	   /* login submit */
});