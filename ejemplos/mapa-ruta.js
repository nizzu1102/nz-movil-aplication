var markers = [];
var map;

function initMap() {
 var origin_place_id = null;
 var destination_place_id = null;
 var travel_mode = google.maps.TravelMode.DRIVING;
 var huancayo = new google.maps.LatLng(-12.056480,-75.225889);
 var mapDiv =document.getElementById('map');
 var geocoder = new google.maps.Geocoder();
 var myOptions = {
  center: huancayo,

  zoom: 18,
  zoomControl: true,
  zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_CENTER
  },
  scaleControl: true,
  mapTypeControl: false,
  streetViewControl: false
}
var map = new google.maps.Map(mapDiv, myOptions);



var marcadores = [
['Puno-real', -12.067889, -75.210783],
['arequipa-brenha', -12.069047, -75.211098],
['ancash-giraldez', -12.068159, -75.209403],
['ancash-lima', -12.068885, -75.208981]
];   
var infowindow = new google.maps.InfoWindow();
var marker, i;
for (i = 0; i < marcadores.length; i++) {  
  marker = new google.maps.Marker({
    position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
    map: map,
    icon:'images/truck_vehicle.png'
  });
  google.maps.event.addListener(marker, 'click', (function(marker, i) {
    return function() {
      infowindow.setContent(marcadores[i][0]);
      infowindow.open(map, marker);
    }
  })(marker, i));
}







  //  geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };   
      map.setCenter(pos);

      var dir="";
      var lat_lng = new google.maps.LatLng(pos.lat, pos.lng);

      var markerLocation = new google.maps.Marker({
        position:pos,
        title:"tu ubicación!",
        draggable: false,
        icon: 'images/location-dot-point.png',
        animation: google.maps.Animation.DROP

      });    
      markerLocation.setMap(map);


      infoWindow = new google.maps.InfoWindow();

      var markerPartida = new google.maps.Marker({
        position: pos,
        draggable: true,
        map: map,
        label: "",
        streetViewControl: false,
        title:"elije tu partida",
        icon: 'images/marker-inicio.png'
      });
      markers.push(markerPartida);


      geocoder.geocode({'latLng': lat_lng }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            var direccion=results[0].formatted_address;
            infoWindow.setContent(direccion);
            infoWindow.open(map, markerPartida);
            $("#origin-input").val(direccion);
          }
        }
      });

      google.maps.event.addListener(markerPartida, 'dragend', function() {

        geocoder.geocode({'latLng': markerPartida.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {

              var direccion =results[0].formatted_address;
              
              infoWindow.setContent(direccion);
              infoWindow.open(map, markerPartida);
              map.panTo(markerPartida.getPosition());
              $("#origin-input").val(direccion);
            }
          }
        });
      });



        ///////////////////////////////////////
        infoWindow2 = new google.maps.InfoWindow();
/*
        var markerLlegada = new google.maps.Marker({
          position: new google.maps.LatLng(-12.064682, -75.212615),
          draggable: true,
          map: map,
          label: "",
          streetViewControl: false,
          title:"Elije tu destino",
          icon: 'images/marker-destino.png',
        });


        geocoder.geocode({'latLng': new google.maps.LatLng(-12.064682, -75.212615) }, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                  //$('#address').val(results[0].formatted_address);
                  //$('#latitude').val(marker.getPosition().lat());
                  //$('#longitude').val(marker.getPosition().lng());
                  infoWindow2.setContent( '<div class="cotizar"><a href="cotizar.html" class="btn-cotizar btnwaves-effect waves-light"><i class="material-icons">monetization_on</i></a></div>'+results[0].formatted_address);
                  infoWindow2.open(map, markerLlegada);
                }
              }
            });
        google.maps.event.addListener(markerLlegada, 'dragend', function() {

          geocoder.geocode({'latLng': markerLlegada.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                    //$('#address').val(results[0].formatted_address);
                    //$('#latitude').val(marker.getPosition().lat());
                    //$('#longitude').val(marker.getPosition().lng());
                    infoWindow2.setContent( '<div class="cotizar"><a href="cotizar.html" class="btn-cotizar btnwaves-effect waves-light"><i class="material-icons">monetization_on</i></a></div>'+results[0].formatted_address);
                    infoWindow2.open(map, markerLlegada);
                    map.panTo(markerLlegada.getPosition());
                  }
                }
              });
        });
        */


        function CenterControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.id = 'goCenterUI';
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '2px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Posición actual';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.id = 'goCenterText';
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '13px';
        controlText.style.lineHeight = '45px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = '<i class="material-icons">near_me</i>';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {


          function moveMarker( map, marker ) {
        //delayed so you can see it move


      };



      markerPartida.setPosition(lat_lng );

      map.panTo(lat_lng);

    });

      }


      var centerControlDiv = document.createElement('div');
      var centerControl = new CenterControl(centerControlDiv, map);

      centerControlDiv.index = 1;
      map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(centerControlDiv);



      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: false,//marcador destino
    map: map,
    panel: document.getElementById('right-panel')
  });
      directionsDisplay.setMap(map);


      var origin_input = document.getElementById('origin-input');
      var destination_input = document.getElementById('destination-input');
      var modes = document.getElementById('mode-selector');

      map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

      var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
      origin_autocomplete.bindTo('bounds', map);

      var destination_autocomplete = new google.maps.places.Autocomplete(destination_input);
      destination_autocomplete.bindTo('bounds', map);

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  /*function setupClickListener(id, mode) {
    var radioButton = document.getElementById(id);
    radioButton.addEventListener('click', function() {
      travel_mode = mode;
    });
  }
  setupClickListener('changemode-walking', google.maps.TravelMode.WALKING);
  setupClickListener('changemode-transit', google.maps.TravelMode.TRANSIT);
  setupClickListener('changemode-driving', google.maps.TravelMode.DRIVING);
  */
  function expandViewportToFitPlace(map, place) {
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
      
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
      
    }
  }

  origin_autocomplete.addListener('place_changed', function() {
    var place = origin_autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }
    expandViewportToFitPlace(map, place);
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];
    // If the place has a geometry, store its place ID and route if we have
    // the other place ID
    origin_place_id = place.place_id;
    route(origin_place_id, destination_place_id, travel_mode,
      directionsService, directionsDisplay);
  });




  destination_autocomplete.addListener('place_changed', function() {
    var place = destination_autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }
    expandViewportToFitPlace(map, place);

    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];


    
    

    var markerLlegada = new google.maps.Marker({
      position: place.geometry.location,
      draggable: false,
      map: map,       
      
      title: place.name,
      icon: 'images/marker-destino.png',
    });
    markers.push(markerLlegada);
    infoWindow.setContent(place.name);
    infoWindow.open(map, markerLlegada);



    // If the place has a geometry, store its place ID and route if we have
    // the other place ID
    destination_place_id = place.place_id;
    route(origin_place_id, destination_place_id, travel_mode,
      directionsService, directionsDisplay);
  });




  function route(origin_place_id, destination_place_id, travel_mode,
   directionsService, directionsDisplay) {
    if (!origin_place_id || !destination_place_id) {
      return;
    }
    directionsService.route({
      origin: {'placeId': origin_place_id},
      destination: {'placeId': destination_place_id},
      travelMode: travel_mode
    }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }








}/*fin get  curretn position*/, function() {
  handleLocationError(true, infoWindow, map.getCenter());
});
} else {
      // geolocalizacion no soportada
      handleLocationError(false, infoWindow, map.getCenter());
    }//fin geolocation







  }//fin initMap




