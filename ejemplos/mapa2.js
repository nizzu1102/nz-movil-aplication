
var map;

var miUbicacion={};
function initMap() {
   var geocoder = new google.maps.Geocoder();
var mapDiv = document.getElementById("map");
  var huancayo = new google.maps.LatLng(-12.056480,-75.225889);
 var myOptions = {
        center:huancayo,
        zoom: 17,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
              },
        scaleControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        draggable: true
      };  
       map = new google.maps.Map(mapDiv, myOptions);

       addCamion();
       locateMe();
       marcadorPartida(ubicacion)

}//end init map
 



function locateMe(){

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(pos){
    miUbicacion.lat=pos.coords.latitude;
    miUbicacion.lng=pos.coords.longitude;
    map.setCenter(miUbicacion);
    map.panTo(miUbicacion);

    //var iconLocation ='images/location-dot-point.png';
   // addMarker(miUbicacion, false, iconLocation);

   //creacion marcador de posicion actual
    var markerMyLocation = new google.maps.Marker({
      position:miUbicacion,
      title:"Estas aquí!",
      draggable: false,
      icon: 'images/location-dot-point.png',
      animation: google.maps.Animation.DROP     
        });    
    markerMyLocation.setMap(map);
    //map.setCenter(myLatlng);



  
    }, function(error){
      alert (error.message);    
    }); //fin get current position

}
 else{
   // geolocalizacion no soportada
      handleLocationError(false, infoWindow, map.getCenter());
    }// finif (navigator.geolocation)

}//fin locateMe

function addCamion(){
//Añadi camines al mapa
      var marcadores = [
        ['Puno-real', -12.067889, -75.210783],
        ['arequipa-brenha', -12.069047, -75.211098],
        ['ancash-giraldez', -12.068159, -75.209403],
        ['ancash-lima', -12.068885, -75.208981],
        ['callao-lima', -12.048908, -77.093886]
      ];   
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < marcadores.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
          map: map,
          icon:'images/truck_vehicle.png'
        });      
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(marcadores[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));   
      }


      function marcadorPartida(ubicacion){
//Marcador Partida
        infoWindow = new google.maps.InfoWindow();
        var markerPartida = new google.maps.Marker({
          position: ubicacion,
          draggable: true,
          map: map,
          label: "",
          streetViewControl: false,
          title:"elije tu partida",
          icon: 'images/marker-inicio.png'
        });

        geocoder.geocode({'latLng': myLatlng }, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              infoWindow.setContent(results[0].formatted_address);
              infoWindow.open(map, markerPartida);
            }
          }
        });

         google.maps.event.addListener(markerPartida, 'dragend', function() {
            geocoder.geocode({'latLng': markerPartida.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infoWindow.setContent(results[0].formatted_address);
                        infoWindow.open(map, markerPartida);
                         map.panTo(markerPartida.getPosition());
                    }
                }
            });
          });
      }
///////fin añadir camiones
/*
//agregar marcadores
function addMarker(ubicacion,draggable,icon){

  var nombreMarker = marker;
  var marker = new google.maps.Marker({
    map:map,
    icon: 'images/location-dot-point.png',
    draggable:draggable,
    position: ubicacion,
    title:' sfsd'+ miUbicacion+'hjj'
  });
 //marker.addListener('click', addInfoWindow)

addInfoWindow(marker.position,marker);
}//fin addMarker


//agregar infowindow 
addInfoWindow = function(ubicacion,nombreMarker) {
  var infoWindow = new google.maps.InfoWindow();
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode({'location': ubicacion}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {

      var placeAddress = results[0].formatted_address;
      var infoContent = placeAddress;
      infoWindow.setContent(infoContent);
      infoWindow.open(map, nombreMarker)

    }
  });





}//fin add window


/*/
}
