
var map;
var infowindow;

function initMap() {
var mapDiv = document.getElementById("map");
var geocoder = new google.maps.Geocoder();


 if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(function(pos) {
  var pos = {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
          radius: pos.coords.accuracy
        };   
 

      var myLatlng = new google.maps.LatLng(pos.lat, pos.lng);
      var radius= pos.radius;

      var myOptions = {
        center:myLatlng,
        zoom: 19,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
              },
        scaleControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        draggable: true
      };  
       map = new google.maps.Map(mapDiv, myOptions);

        //marcador de ubicaión actual
        var markerLocation = new google.maps.Marker({
          position:myLatlng,
          title:"tu ubicación!",
          draggable: false,
          icon: 'images/location-dot-point.png',
          animation: google.maps.Animation.DROP     
            });    
        markerLocation.setMap(map);
        map.setCenter(myLatlng);

        //Marcador Partida
        infoWindow = new google.maps.InfoWindow();
        var markerPartida = new google.maps.Marker({
          position: pos,
          draggable: true,
          map: map,
          label: "",
          streetViewControl: false,
          title:"elije tu partida",
          icon: 'images/marker-inicio.png'
        });

        geocoder.geocode({'latLng': myLatlng }, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              infoWindow.setContent(results[0].formatted_address);
              infoWindow.open(map, markerPartida);
            }
          }
        });

         google.maps.event.addListener(markerPartida, 'dragend', function() {
            geocoder.geocode({'latLng': markerPartida.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infoWindow.setContent(results[0].formatted_address);
                        infoWindow.open(map, markerPartida);
                         map.panTo(markerPartida.getPosition());
                    }
                }
            });
          });


        //Fin marcador Partida
        //inicio marcador llegada
        infoWindow2 = new google.maps.InfoWindow(); 
        var markerLlegada = new google.maps.Marker({
          position: new google.maps.LatLng(-12.063308, -75.215756),
          draggable: true,
          map: map,
          label: "",
          streetViewControl: false,
          title:"Elije tu destino",
          icon: 'images/marker-destino.png',
         });

        var markerLlegadaLatLng=markerLlegada.getPosition();
        geocoder.geocode({'latLng': new google.maps.LatLng(-12.063308, -75.215756) }, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {               
              infoWindow2.setContent( '<div class="cotizar"><a href="#" class="btn-cotizar btnwaves-effect waves-light"><i class="material-icons">monetization_on</i></a>'+results[0].formatted_address+'</div>');
              infoWindow2.open(map, markerLlegada);
              }
            }
        });
  
        google.maps.event.addListener(markerLlegada, 'dragend', function() {
        geocoder.geocode({'latLng': markerLlegada.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                infoWindow2.setContent( '<div class="cotizar"><a href="cotizar.html" class="btn-cotizar btnwaves-effect waves-light"><i class="material-icons">monetization_on</i></a></div>'+results[0].formatted_address);
                infoWindow2.open(map, markerLlegada);
                map.panTo(markerLlegada.getPosition());
                markerLlegada.setPosition( map .getCenter() );
              }
            }
          });
        });
  //fin amrcador lelgada
  // centrar mapa creando control
      function CenterControl(controlDiv, map) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.id = 'goCenterUI';
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '2px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Posición actual';
        controlDiv.appendChild(controlUI);
        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.id = 'goCenterText';
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '13px';
        controlText.style.lineHeight = '45px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = '<i class="material-icons">my_location</i>';
        controlUI.appendChild(controlText);
        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {        
      function moveMarker( map, marker ) {//delayed so you can see it move  
      };
         markerPartida.setPosition(myLatlng );
         map.panTo(myLatlng);
        });
      }

      var centerControlDiv = document.createElement('div');
      var centerControl = new CenterControl(centerControlDiv, map);
      centerControlDiv.index = 1;
      map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(centerControlDiv);
  //fin creacion control 


//Añadi camines al mapa
      var marcadores = [
        ['Puno-real', -12.067889, -75.210783],
        ['arequipa-brenha', -12.069047, -75.211098],
        ['ancash-giraldez', -12.068159, -75.209403],
        ['ancash-lima', -12.068885, -75.208981],
        ['callao-lima', -12.048908, -77.093886]
      ];   
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
      for (i = 0; i < marcadores.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
          map: map,
          icon:'images/truck_vehicle.png'
        });      
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(marcadores[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));   
      }
///////fin añadir camiones



  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
     var datosInput=place.geometry.location; 
     // alert (datosInput);$("#pac-input").val();
 
//var niels2 = ;
//alert (niels2);
  var directionsDisplay = new google.maps.DirectionsRenderer({
          map: map,
          suppressMarkers: true,
          draggable:false
        });

// Pass the directions request to the directions service.
        var directionsService = new google.maps.DirectionsService();
        directionsDisplay.setMap(map);
         directionsDisplay.setPanel(document.getElementById("right-panel"));

// function calcRoute() {
        // Set destination, origin and travel mode.
        var request = {
          origin: markerPartida.getPosition(),
          destination:  datosInput,
          travelMode: 'DRIVING'
                  };
        
        directionsService.route(request, function(response, status) {
          if (status == 'OK') {
            // Display the route on the map.
            directionsDisplay.setDirections(response);
               markerPartida.setVisible(false); // maps API hide call
                markerLlegada.setVisible(false); // maps API hide call
              //infoWindow2.close(map, markerLlegada);
              //infoWindow.close(map, markerPartida);
          }
        });
//}calcRoute();

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.location) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);

  });
  // [END region_getplaces]





  });// end getCurrentPosition
 }
 else{
   // geolocalizacion no soportada
      handleLocationError(false, infoWindow, map.getCenter());
    }// finif (navigator.geolocation)


}//end init map
 




